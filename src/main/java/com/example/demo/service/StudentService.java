package com.example.demo.service;

import com.example.demo.message.ResponseMessage;
import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    private final FileService fileService;

    @Autowired
    public StudentService(StudentRepository studentRepository, FileService fileService) {
        this.studentRepository = studentRepository;
        this.fileService = fileService;
    }

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    public ResponseEntity<ResponseMessage> uploadFile(MultipartFile file) throws IOException {

        try {
            fileService.uploadFile(file);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Error uploading file."));
        }

        fileService.saveStudentsFromFile(file, studentRepository);

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Students created successfully."));
    }
}
