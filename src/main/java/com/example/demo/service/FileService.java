package com.example.demo.service;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileService {

    public void uploadFile(MultipartFile file) throws IOException {
        File convertFile = new File(file.getOriginalFilename());
        convertFile.createNewFile();

        try (FileOutputStream fileOutputStream = new FileOutputStream(convertFile)) {
            fileOutputStream.write(file.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveStudentsFromFile(MultipartFile file, StudentRepository studentRepository) throws IOException {
        FileInputStream newFile = new FileInputStream(new File(file.getOriginalFilename()));
        Workbook workbook = new XSSFWorkbook(newFile);

        Sheet sheet = workbook.getSheetAt(0);

        for (Row row : sheet) {

            Student student = new Student(
                    row.getCell(0).getRichStringCellValue().getString(),
                    row.getCell(1).getRichStringCellValue().getString(),
                    row.getCell(2).getRichStringCellValue().getString(),
                    (int) row.getCell(3).getNumericCellValue()
            );

            studentRepository.save(student);
        }

        File oldFile = new File(file.getOriginalFilename());
        oldFile.delete();
    }
}
