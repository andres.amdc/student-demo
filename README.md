This project is a simple demo of .xslx file handling. It's a REST API with two endpoints:

Get Students
- Returns a JSON object with the Students stored in the DB.

Upload File
- Receives an .xslx file which contains rows with students info (name, lastName, email, age).
- Process the .xslx file by reading each row and storing the student in the DB.
- Returns an OK Response.

For a detailed understanding of the process, please watch the demo video.

https://drive.google.com/drive/folders/1PrWwnNgqCplaZBmN5xph5xLlOJKmtgVQ?usp=sharing
